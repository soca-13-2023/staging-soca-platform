function getValue(key, valueDefault = '') {
    const value = localStorage.getItem(key);
    return value != null ? value : valueDefault;
}

function setValue(key, value) {
    localStorage.setItem(key, value);
}

export { getValue, setValue }