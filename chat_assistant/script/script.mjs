import { ChatService } from './services/service.mjs'
import { ChatPrompt } from "./ui/chatPrompt.mjs";
import { ChatList } from "./ui/chatList.mjs";
import { ChatSender } from "./enum.mjs";
import { getValue } from '../../preferences/preferences.mjs';

const BASE_URL = 'http://localhost:5438/';

// ChatService: Service untuk menghubungkan ke Server
// ChatPrompt: Elemen untuk input chat
// ChatList: Elemen untuk menampilkan semua chat

/*  
    Format data chat:
    {
        id <- Digunakan untuk referensi bagi ChatService
        data: {
            text: <- Isi teks chat
            voices: <- Isi voice chat (berbentuk array)
            links: <- isi lampiran link chat (berbentuk array)
            files: <- isi lampiran file chat (berbentuk array)
        }
    }
*/

// Untuk tampilan lampiran (voice, link, file) terletak di folder 'ui/attachment'
// Apabila ingin merubah atau menambah tampilan lampiran, maka rubah disitu


document.addEventListener('DOMContentLoaded', _ => {
    const chatService = new ChatService(BASE_URL);
    const chatPrompt = new ChatPrompt();
    const chatList = new ChatList();

    // Daftar semua chat yang sedang loading.
    // User tidak boleh mengirimkan chat apa-apa apabila masih ada chat yang sedang loading.
    const ongoingChat = [];

    // User mengirimkan chat.
    chatPrompt.addEventListener('send', (ev) => {
        const request = ev.detail;

        // Saat ini Service (ChatService) hanya dapat mengirimkan chat teks biasa, atau chat voice.
        // Service belum memiliki fungsi untuk mengirimkan chat berupa file dan link.
        //
        // Catatan: Walaupun ChatPrompt mampu mengirimkan chat voice lebih dari satu,
        // Service hanya akan menggunakan elemen pertama dari daftar chat voice untuk dikirim.

        if (request.data.voices.length > 0) { // Apabila User mengirimkan chat voice
            // Buat chat bubble loading sembari menunggu transkrip voice dari Server.
            // Chat bubble ini nantinya akan diisi dengan transkrip voice dari Server.
            console.log('Sending chat voice..');
            chatList.addChat(request.id, ChatSender.User, null);
            chatService.sendChatAudio(request.id, request.data);
            // Tambahkan ID chat ini ke daftar chat yang sedang loading
            ongoingChat.push(request.id);
        } else { // Apabila User mengirimkan chat teks biasa
            console.log('Sending chat text..');
            chatList.addChat(request.id, ChatSender.User, request.data);
            chatService.sendChatText(request.data);
        }

        // Blokir tombol send apabila ada chat yang sedang loading
        if (ongoingChat.length == 0) {
            chatPrompt.openSendButtonBlock();
        } else {
            chatPrompt.blockSendButton();
        }
    });

    // Server mengirimkan jawaban dari chat yang dikirimkan User.
    chatService.addEventListener('message', (ev) => {
        const response = ev.detail;        
 
        if (response.data == null) {
            // Apabila Server mengirimkan chat loading,
            // blokir tombol send dengan menambahkan ID chat ini ke daftar chat yang sedang loading
            ongoingChat.push(response.id);
        } else {
            // Apabila sebelumnya Server pernah mengirimkan chat loading dengan ID chat ini,
            // maka hapus ID chat ini dari daftar chat yang sedang loading
            const ongoingChatMatch = ongoingChat.findIndex(x => x == response.id);
            if (ongoingChatMatch != null) {
                ongoingChat.splice(ongoingChatMatch, 1);
            }
        }

        // Tambahkan/Beri isi chat bubble untuk Server ke elemen chat-list di HTML.
        chatList.addChat(response.id, null, response.data);

        // Blokir tombol send apabila ada chat yang sedang loading
        if (ongoingChat.length == 0) {
            chatPrompt.openSendButtonBlock();
        } else {
            chatPrompt.blockSendButton();
        }
    });

    setAssistantProfile();
});

function setAssistantProfile() {
    const assistantNameText = document.querySelector('.assistant-list-item p');
    const assistantPictureImage = document.querySelector('.assistant-list-item img');

    const assistantName = getValue('assistant-name', 'm-assistant');
    const assistantPicture = getValue('assistant-picture');

    assistantNameText.innerText = assistantName;

    if (assistantPicture != '') {
        assistantPictureImage.src = assistantPicture;
    }
}