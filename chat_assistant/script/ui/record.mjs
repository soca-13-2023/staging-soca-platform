class VoiceRecorder extends EventTarget {
    audioContext;
    gumStream;

    start() {
        const thisClass = this;

        /*
            Simple constraints object, for more advanced audio features see
            https://addpipe.com/blog/audio-constraints-getusermedia/
        */
        var constraints = { audio: true, video: false }

        /*
            We're using the standard promise based getUserMedia() 
            https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        */
        navigator.mediaDevices.getUserMedia(constraints)
            .then((stream) => {
                console.log("getUserMedia() success, stream created, initializing Recorder.js ...");
                // Assign to gumStream for later use.
                this.gumStream = stream;
        
                // Create an audio context after getUserMedia is called.
                // sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods.
                // the sampleRate defaults to the one set in your OS for your playback device.
                this.audioContext = new AudioContext();
        
                // Create audio source.
                const audioSource = this.audioContext.createMediaStreamSource(stream);
                // Create analyzer to process audio data from audio source.
                const analyser = this.audioContext.createAnalyser();
                const scriptProcessor = this.audioContext.createScriptProcessor(2048, 1, 1);
                analyser.fftSize = 1024;

                audioSource.connect(analyser);
                analyser.connect(scriptProcessor);
                scriptProcessor.connect(this.audioContext.destination);

                // Create the Recorder object and configure to record mono sound (1 channel)
                // Recording 2 channels  will double the file size
                const recorder = new Recorder(audioSource, { numChannels: 1 });

                // Listen for data processed from audio source.
                let counter_stop = null;
                scriptProcessor.onaudioprocess = () => {
                    const array = new Uint8Array(analyser.frequencyBinCount);
                    analyser.getByteFrequencyData(array);

                    const average = array.reduce((a, b) => a + b, 0) / array.length;
                    const volume = average > 20 ? 100 : (100 / 20) * average;

                    thisClass.dispatchEvent(new CustomEvent('recording', { detail: { volume: volume } }));

                    if (volume >= 85 && counter_stop != null) {
                        counter_stop = null;
                    }
    
                    // End recording if the User didn't speak anything for 3 seconds.
                    if (volume < 85) {
                        if (counter_stop == null) {
                            counter_stop = new Date(Date.now() + 3000).toISOString()
                        }

                        console.warn('BELOW', counter_stop);
    
                        var now = new Date().toISOString()
                        if (now > counter_stop) {
                            this.stop();
                        }
                    }
                }; 

                this.audioContext.addEventListener('statechange', () => {
                    if (this.audioContext.state == 'closed') {
                        // Export the recoded audio to WAV and then return the audio to the caller
                        recorder.stop();
                        recorder.exportWAV((blob) => {
                            thisClass.dispatchEvent(new CustomEvent('finish', { detail: { audio: blob } }));
                        });
                    }
                })
                
                recorder.record();

            }).catch((err) => {
                this.stop();
                thisClass.dispatchEvent(new CustomEvent('error', { detail: { error: err } }));
                console.log('Recording failed', err);
            });
    } 

    stop() {
        // Stop microphone access
        this.audioContext.close();
        this.gumStream.getAudioTracks()[0].stop();
    }
}

export { VoiceRecorder }