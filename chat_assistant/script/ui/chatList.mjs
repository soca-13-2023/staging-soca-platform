import { ChatBubble } from "./chatBubble.mjs"

export class ChatList {
    chatList = []; // Daftar chat buble
    chatListView; // Elemen HTML untuk menampilkan semua chat bubble

    constructor() {
        this.chatListView = document.querySelector('.chat-list');
    }

    // Buat chat bubble dengan Id atau data yang diberikan.
    // Jika data kosong, maka tampilkan indikator loading.
    //
    // Jika chat bubble dengan Id yang diberikan sudah pernah dibuat,
    // maka isi chat bubble tersebut dengan data yang diberikan.
    //
    // Catatan: Walaupun namanya 'addChat' namun dapat digunakan untuk mengisi isi chat bubble.
    addChat(id, from, data = null) {
        const chatBubble = this.chatList.find(x => x.id == id);
        if (!chatBubble) {
            const chatBubble = new ChatBubble(id, from, data == null);
            this.chatList.push(chatBubble);
            this.chatListView.append(chatBubble.element.root);

            // Jika data tidak kosong, maka langsung isi chat tersebut.
            if (data != null) {
                chatBubble.fill(data.text);
            }
        } else {
            chatBubble.fill(data.text, data.audio);
        }
    }
}