function openDialog(dialogClassName) {
    const dialog = document.querySelector('.' + dialogClassName);
    if (dialog == null) throw new Error('Tidak ada dialog dengan class yang diberikan');

    gsap.fromTo(dialog, {
        display: 'grid',
        opacity: 0
    }, {
        opacity: 1,
        duration: .25
    });
}

function closeDialog(dialogClassName) {
    const dialog = document.querySelector('.' + dialogClassName);
    if (dialog == null) throw new Error('Tidak ada dialog dengan class yang diberikan');

    gsap.to(dialog, {
        opacity: 0,
        display: 'none',
        duration: .25
    });
}

export { openDialog, closeDialog }