function setupTooltip(action) {
    const tooltip = action.querySelector('.chat-prompt-action-tooltip');
    if (tooltip == null) return;

    action.addEventListener('mouseenter', _ => {
        tooltip.style.display = 'block';
        const left = action.offsetLeft - (tooltip.clientWidth / 2) + (action.clientWidth / 2);
        tooltip.style.left = left > window.innerWidth ? tooltip.clientWidth + 'px' : left + 'px';
        tooltip.style.top = action.offsetTop - 20 + 'px';
        gsap.fromTo(tooltip, {
            opacity: 0
        }, {
            opacity: 1,
            duration: .25
        });
    })
    action.addEventListener('mouseleave', _ => {
        gsap.to(tooltip, {
            opacity: 0,
            display: 'none',
            duration: .25
        });
    })
}

export { setupTooltip }