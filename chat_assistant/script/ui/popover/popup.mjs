function setupPopupFromAction(action, popupClassName) {
    const popup = document.querySelector('.' + popupClassName);
    const exitZone = document.createElement('div');
    exitZone.classList.add('chat-prompt-action-popup-exit');

    action.addEventListener('click', _ => {
        openPopup(action, popup);

        exitZone.addEventListener('click', _ => {
            closePopup(popup);
        })
        document.body.append(exitZone);
    });

    function openPopup() {
        popup.style.display = 'flex';
        const left = action.offsetLeft - (popup.clientWidth / 2) + (action.clientWidth / 2);
        popup.style.left = left > window.innerWidth ? popup.clientWidth + 'px' : left + 'px';
        popup.style.top = action.offsetTop - popup.clientHeight + 15 + 'px';
        gsap.fromTo(popup, {
            transform: `translateY(${popup.clientHeight}px)`,
            'clip-path': `inset(0px 0px ${popup.clientHeight}px 0px)`
        }, {
            transform: 'translateY(0px)',
            'clip-path': `inset(0px 0px 0px 0px)`,
            duration: .25
        });
    }
    function closePopup() {
        exitZone.remove();
        gsap.to(popup, {
            transform: `translateY(${popup.clientHeight}px)`,
            'clip-path': `inset(0px 0px ${popup.clientHeight}px 0px)`,
            display: 'none',
            duration: .25
        });
    }

    // Untuk menutup popup nanti
    popup.close = closePopup;

    return popup;
}

export { setupPopupFromAction }