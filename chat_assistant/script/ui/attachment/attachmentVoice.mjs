import { BaseAttachment } from "./attachmentBase.mjs";
import WaveSurfer from 'https://unpkg.com/wavesurfer.js@7/dist/wavesurfer.esm.js'

export class VoiceAttachment extends BaseAttachment {
    isPlaying = false;
    audio;
    player;
    element = {};

    constructor(audio) {
        super('voice');
        this.audio = audio;

        const root = document.createElement('div');
        const togglePlaybackButton = document.createElement('button');
        const waveform = document.createElement('div');
        const durationText = document.createElement('p');
        const removeButton = document.createElement('button');

        root.classList.add('attachment-voice');
        togglePlaybackButton.classList.add('attachment-voice-toggle-button');
        waveform.classList.add('attachment-voice-waveform');
        durationText.classList.add('attachment-voice-duration');
        removeButton.classList.add('attachment-voice-remove-button');

        // Ketika tombol play/pause diklik
        togglePlaybackButton.addEventListener('click', () => {
            if (this.isPlaying) this.pause();
            else this.play();
        });

        // Hapus lampiran voice
        removeButton.addEventListener('click', () => {
            this.dispatchEvent(new Event('remove'));
        });

        const audioURL = URL.createObjectURL(this.audio, { type: 'audio/wav' });
        this.player = WaveSurfer.create({
            container: waveform,
            waveColor: '#fff',
            barHeight: 40,
            height: 40,
            url: audioURL
        })
        this.player.on('ready', () => {
            const totalSeconds = Math.round(this.player.getDuration());
            const minutes = Math.floor(totalSeconds / 60);
            const seconds = totalSeconds % 60;
    
            function padTo2Digits(num) {
                return num.toString().padStart(2, '0');
            }
    
            const result = `${padTo2Digits(minutes)}:${padTo2Digits(seconds)}`;
            durationText.innerText = result;
        });
        this.player.on('interaction', () => {
            this.player.play();
        });
        this.player.on('play', () => {
            this.isPlaying = true;
            this.element.togglePlaybackButton.classList.add('playing');
        });
        this.player.on('pause', () => {
            this.isPlaying = false;
            this.element.togglePlaybackButton.classList.remove('playing');
        });
        this.player.on('finish', () => {
            this.isPlaying = false;
            this.element.togglePlaybackButton.classList.remove('playing');
        });

        root.append(togglePlaybackButton, waveform, durationText, removeButton);
        this.element = { root, togglePlaybackButton, waveform, durationText, removeButton };
    }

    play() {
        this.player.play();
    }

    pause() {
        this.player.pause();
    }
}