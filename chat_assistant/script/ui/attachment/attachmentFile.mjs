import { BaseAttachment } from "./attachmentBase.mjs";

export class AttachmentFile extends BaseAttachment {
    file;
    name = '';
    size = 0;
    element = {};

    constructor(file, type, name, size) {
        super('link');

        const root = document.createElement('div');
        const banner = document.createElement('div');
        const bannerText = document.createElement('p');
        const fileNameText = document.createElement('p');
        const fileSizeText = document.createElement('p');
        const removeButton = document.createElement('button');

        root.classList.add('attachment-file');
        banner.classList.add('attachment-file-banner');
        fileNameText.classList.add('attachment-file-name');
        fileSizeText.classList.add('attachment-file-size');
        removeButton.classList.add('attachment-file-remove-button');

        bannerText.innerText = type.split('/')[1];
        fileNameText.innerText = name;
        fileSizeText.innerText = size;

        // Hapus lampiran voice
        removeButton.addEventListener('click', () => {
            this.dispatchEvent(new Event('remove'));
        });

        banner.append(bannerText);
        root.append(banner, fileNameText, fileSizeText, removeButton);
        this.element = { root, banner, bannerText, fileNameText, fileSizeText, removeButton };
    }
}