export class BaseAttachment extends EventTarget {
    type = '';

    constructor(type) {
        super();
        this.type = type;
    }
}