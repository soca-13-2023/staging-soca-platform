import { BaseAttachment } from "./attachmentBase.mjs";

export class AttachmentLink extends BaseAttachment {
    isUploading = true;
    element = {};

    constructor(url, description) {
        super('link');

        const root = document.createElement('div');
        const banner = document.createElement('div');
        const bannerText = document.createElement('p');
        const urlText = document.createElement('a');
        const descriptionText = document.createElement('p');
        const loadingIndicator = document.createElement('img');
        const removeButton = document.createElement('button');

        root.classList.add('attachment-link');
        banner.classList.add('attachment-link-banner');
        urlText.classList.add('attachment-link-url');
        descriptionText.classList.add('attachment-link-description');
        loadingIndicator.classList.add('attachment-link-loading');
        removeButton.classList.add('attachment-link-remove-button');

        bannerText.innerText = 'URL';
        urlText.innerText = url;
        urlText.href = url;
        descriptionText.innerText = description;
        loadingIndicator.src = 'assets/loading.svg'
        
        // Hapus lampiran voice
        removeButton.addEventListener('click', () => {
            this.dispatchEvent(new Event('remove'));
        });
        
        this.upload(url);

        banner.append(bannerText);
        root.append(banner, urlText, descriptionText, loadingIndicator, removeButton);
        this.element = { root, banner, bannerText, urlText, descriptionText, loadingIndicator, removeButton };
    }

    // Fungsi untuk proses upload link
    // Belum dibuat
    upload(url) {
        this.isUploading = true;
        // Kosong
    }
}