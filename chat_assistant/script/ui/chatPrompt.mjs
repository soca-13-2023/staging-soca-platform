import { setupTooltip } from "./popover/tooltip.mjs";
import { setupPopupFromAction } from "./popover/popup.mjs";
import { setupDialog } from "./popover/dialog.mjs";
import { VoiceRecorder } from "./record.mjs";
import { VoiceAttachment } from "./attachment/attachmentVoice.mjs";
import { AttachmentLink } from "./attachment/attachmentLink.mjs";
import { AttachmentFile } from "./attachment/attachmentFile.mjs";
import { getValue } from "../../../preferences/preferences.mjs";

const ChatPromptMode = {
    'NoAttachment': 0, // Tampilan awal prompt, atau tidak ada lampiran apa-apa
    'WithAttachment': 1, // Tampilan saat ada lampiran
    'VoiceRecording': 2 // Tampilan saat User sedang merekam voice untuk voice generate
}

export class ChatPrompt extends EventTarget {
    // Variabel inti
    chatPromptMode = ChatPromptMode.NoAttachment;
    chatAttachmentList = []; // Daftar semua lampiran (seperti: voice, file, link)
    isSendBlockedExternally = false; // Apabila pihak luar (bukan berasal dari class ini) memblokir tombol send
    
    // HTML Element
    chatInput;
    chatActionList = []; // Tombol link, file, voice, send
    chatAttachmentListView; // Elemen HTML untuk menampilkan semua lampiran
    recordStopButton;
    
    // Variable recorder
    recorder;
    isRecording = false;
    isRecordDirectSend = false; // Digunakan apabila User menekan tombol send saat tengah merekam voice (tidak menekan tombol stop)

    constructor() {
        super(); const thisClass = this;
        
        // Prepare untuk chatInput
        this.chatInput = document.querySelector('.chat-prompt-input');
        // Atur placeholder berdasarkan greeting dari setting
        this.chatInput.placeholder = getValue('assistant-greeting', 'Tanyakan apa saja tentang bisnismu hari ini..');
        this.chatInput.addEventListener('input', (ev) => {
            // Matikan tombol send apabila input kosong dan tidak ada lampiran apa-apa
            if (ev.target.value == '' && this.chatAttachmentList.length == 0) {
                this.#blockSendButtonInternal();
            } else {
                this.#openSendButtonBlockInternal();
            }
        });
        this.chatInput.addEventListener('keypress', (ev) => {
            if (ev.key == 'Enter') {
                this.chatActionList[3].click();
            }
        });

        this.chatAttachmentListView = document.querySelector('.chat-prompt-attachment-list');
        
        // Prepare untuk tombol link, file, voice, dan send
        this.chatActionList = document.querySelectorAll('.chat-prompt-action');
        this.chatActionList.forEach(action => {
            setupTooltip(action);

            if (action.classList.contains('action-link')) {
                setupTooltip(action)
                setupLinkAction(action);
            }
            if (action.classList.contains('action-file')) {
                setupTooltip(action);
                setupFileAction(action);
            }
            if (action.classList.contains('action-voice')) {
                setupVoiceAction(action, 'action-voice-popup');
            }
            if (action.classList.contains('action-send')) {
                setupSendAction(action);
            }
        });

        // Siapkan tombol stop recording untuk digunakan saat rekaman untuk generate voice.
        this.recordStopButton = document.querySelector('.chat-prompt-action-stop-record');
        this.recordStopButton.addEventListener('click', () => this.stopGenerateVoice());


        function setupLinkAction(action) {
            const popup = setupPopupFromAction(action, 'action-link-popup');
            const linkInput = popup.querySelector('.action-link-input');
            const linkSendButton = popup.querySelector('.action-link-send');

            linkInput.addEventListener('input', (ev) => {
                // Matikan tombol submit link apabila link kosong
                if (ev.target.value == '') {
                    linkSendButton.disabled = true;
                } else {
                    linkSendButton.disabled = false;
                }
            });
            linkInput.addEventListener('keypress', (ev) => {
                if (ev.key == 'Enter') {
                    linkSendButton.click();
                }
            });
            linkSendButton.addEventListener('click', () => {
                thisClass.attachLink(linkInput.value);
                linkInput.value = '';
                popup.close();
            });
        }
        
        function setupFileAction(action) {
            const fileInput = document.querySelector('.action-file-input');
            fileInput.addEventListener('change', (ev) => thisClass.attachFiles(ev.target.files));

            action.addEventListener('click', () => {
                fileInput.click();
            });
        }
        
        function setupVoiceAction(action) {
            const popup = setupPopupFromAction(action, 'action-voice-popup');
            const voiceGenerateButton = popup.querySelector('.action-voice-popup-generate');
            const voiceCloneButton = popup.querySelector('.action-voice-popup-clone');
            const voiceDubbingButton = popup.querySelector('.action-voice-popup-dubbing');
        
            voiceGenerateButton.addEventListener('click', () => {
                thisClass.startGenerateVoice(); 
                thisClass.#openSendButtonBlockInternal(); 
                popup.close(); 
            });
            voiceCloneButton.addEventListener('click', () => {
                alert('Belum dibuat');
                popup.close(); 
            });
            voiceDubbingButton.addEventListener('click', () => {
                alert('Belum dibuat');
                popup.close(); 
            });
        } 

        function setupSendAction(action) {
            action.addEventListener('click', () => {
                // Apabila User sedang merekam untuk generate voice,
                // langsung kirim audio yang direkam.
                if (thisClass.isRecording) {
                    thisClass.isRecordDirectSend = true;
                    thisClass.stopGenerateVoice();
                } else {
                    thisClass.sendChat()
                }
            });
        }
    }

    // Mulai merekam saat User menekan tombol Voice Generation
    startGenerateVoice() {
        // Ganti mode input ke recording mode (double) dengan menghilangkan semua chat action.
        // Munculkan tombol stop recording setelah selesai mengganti mode input.
        this.changePromptMode(ChatPromptMode.VoiceRecording);

        // Prepare untuk waveform
        const waveform = document.querySelector('.chat-prompt-audio-recording');
        const ctx = waveform.getContext('2d');
        const waveformHeight = waveform.clientHeight;
        const waveformCenterY = waveform.clientHeight / 2;
        let waveformDrawX = 1;

        waveform.width = waveform.clientWidth;
        waveform.height = waveform.clientHeight;
        ctx.fillStyle = 'white';
    
        // Mulai recording
        this.recorder = new VoiceRecorder();
        this.recorder.addEventListener('recording', (ev) => {
            const volume = ev.detail.volume;
            const barHeight = volume / 100 * waveformHeight;

            // Apabila waveform sudah penuh digambar, hapus semua dan balik lagi ke titik X = 1
            if (waveformDrawX > waveform.width) {
                ctx.clearRect(0, 0, waveform.width, waveform.height);
                waveformDrawX = 1;
            }

            ctx.fillRect(waveformDrawX, waveformCenterY - (barHeight / 2), 2, barHeight);
            waveformDrawX += 4;
        });
        this.recorder.addEventListener('finish', (ev) => {
            const audio = ev.detail.audio;
            this.isRecording = false;
            this.addAttachment(new VoiceAttachment(audio));

            if (this.isRecordDirectSend) {
                this.sendChat();
            } else {
                this.changePromptMode(ChatPromptMode.WithAttachment);
            }
        });
        this.recorder.addEventListener('error', (error) => {
            this.stopGenerateVoice();
        })
        this.recorder.start();
        this.isRecording = true;
    }

    // Hentikan proses perekaman voice generate
    // Digunakan oleh tombol stop untuk menghentikan perekaman
    stopGenerateVoice() {
        this.recorder.stop();
    }

    // Saat User mencoba melampirkan link
    attachLink(url) {
        this.changePromptMode(ChatPromptMode.WithAttachment);
        this.addAttachment(new AttachmentLink(url, 'Abcdefg'));
    }

    // Saat User mencoba melampirkan file
    attachFiles(files) {
        console.log('files', files);
        this.changePromptMode(ChatPromptMode.WithAttachment);

        let fileSizeInMb = 0;
        for (const file of files) {
            fileSizeInMb += file.size;
        }
        fileSizeInMb /= 1000000;
        
        if (fileSizeInMb > 10) {
            return alert('File size too large (max 10 mb)');
        }

        for (const file of files) {
            this.addAttachment(new AttachmentFile(file, file.type, file.name, file.size));
        }
    }

    // Tambahkan lampiran ke daftar lampiran
    addAttachment(attachment) {
        this.chatAttachmentListView.append(attachment.element.root);
        this.chatAttachmentList.push(attachment);

        // Hapus lampiran saat tombol 'hapus' diklik
        attachment.addEventListener('remove', () => {
            const attachmentIndex = this.chatAttachmentList.findIndex(x => x == attachment);
            this.chatAttachmentList.splice(attachmentIndex, 1);
            attachment.element.root.remove();

            if (this.chatAttachmentList.length == 0) {
                // Ganti mode input menjadi satu baris (single) kembali.
                this.changePromptMode(ChatPromptMode.NoAttachment);
            }
        })
    }

    // Hapus semua lampiran yang ada
    removeAllAttachment() {
        this.chatAttachmentList.forEach(attachment => {
            attachment.element.root.remove();
        });
        this.chatAttachmentList = [];
    }
    
    // Ganti mode prompt
    changePromptMode(mode, animFinishedCallback = undefined) {
        if (mode == this.chatPromptMode) return; const thisClass = this;
    
        if (mode == ChatPromptMode.NoAttachment) {
            if (this.chatPromptMode == ChatPromptMode.VoiceRecording) exitVoiceRecording();
            switchToNoAttachment(animFinishedCallback);
        }
        if (mode == ChatPromptMode.WithAttachment) {
            if (this.chatPromptMode == ChatPromptMode.VoiceRecording) exitVoiceRecording();
            switchToWithAttachment(animFinishedCallback);
        }
        if (mode == ChatPromptMode.VoiceRecording) {
            if (thisClass.chatPromptMode == ChatPromptMode.NoAttachment) switchToWithAttachment();
            enterVoiceRecording(animFinishedCallback);
        }

        this.chatPromptMode = mode;
    
        function switchToNoAttachment(animFinishedCallback2 = undefined) {
            const prompt = document.querySelector('.chat-prompt');
            const heightBefore = prompt.clientHeight;
            prompt.classList.remove('chat-prompt-double-mode')
            // Animasikan prompt height.
            gsap.fromTo(prompt, {
                height: heightBefore + 'px'
            }, {
                height: 'auto',
                onComplete: () => {
                    if (animFinishedCallback2 != undefined) animFinishedCallback2();
                }
            });
        }
    
        function switchToWithAttachment(animFinishedCallback2 = undefined) {
            const prompt = document.querySelector('.chat-prompt');
            const promptAttachmentList = document.querySelector('.chat-prompt-attachment-list');
            const heightBefore = prompt.clientHeight;
            prompt.classList.add('chat-prompt-double-mode');

            // Animasikan prompt height.
            gsap.fromTo(prompt, {
                height: heightBefore + 'px'
            }, {
                height: 'auto'
            });

            promptAttachmentList.style.display = 'flex';
        }

        function enterVoiceRecording(animFinishedCallback2 = undefined) {
            const promptAttachmentList = document.querySelector('.chat-prompt-attachment-list');
            promptAttachmentList.style.display = 'none';

            const waveform = document.querySelector('.chat-prompt-audio-recording');
            waveform.style.display = 'block';

            thisClass.chatActionList.forEach((action, index) => {
                if (index == 3) return; // Skip tombol send.
    
                gsap.to(action, {
                    display: 'none',
                    opacity: 0,
                    duration: .5,
                    onComplete: () => {
                        gsap.fromTo(thisClass.recordStopButton, {
                            display: 'block',
                            transform: 'scale(0)'
                        }, {
                            transform: 'scale(1)',
                            duration: .25,
                            onComplete: () => {
                                if (animFinishedCallback2 != undefined) animFinishedCallback2(); 
                            }
                        });
                    }
                })
            });
        }
        function exitVoiceRecording(animFinishedCallback2 = undefined) {
            const waveform = document.querySelector('.chat-prompt-audio-recording');
            waveform.style.display = 'none';

            gsap.to(thisClass.recordStopButton, {
                display: 'none',
                transform: 'scale(0)',
                duration: .25,
                onComplete: () => {
                    thisClass.chatActionList.forEach((action, index) => {
                        if (index == 3) return; // Skip tombol send.
                        gsap.fromTo(action, {
                            display: 'block'
                        }, {
                            opacity: 1,
                            duration: .5,
                            onComplete: () => {
                                if (animFinishedCallback2 != undefined) animFinishedCallback2(); 
                            }
                        })
                    });
                }
            }); 
        }
    }

    // Buka blokir tombol send
    openSendButtonBlock() {
        this.isSendBlockedExternally = false;
        if (this.chatInput.value != '')
            this.chatActionList[3].disabled = false;
    }
    // Blokir tombol send
    blockSendButton() {
        this.isSendBlockedExternally = true;
        this.chatActionList[3].disabled = true;
    }

    // Buka blokir tombol send
    #openSendButtonBlockInternal() {
        if (!this.isSendBlockedExternally)
            this.chatActionList[3].disabled = false;
    }
    // Blokir tombol send
    #blockSendButtonInternal() {
        this.chatActionList[3].disabled = true;
    }

    // Kirim pesan
    sendChat() {
        // Apabila User sedang merekam voice dan langsung menekan tombol send
        // tanpa distop terlebih dahulu, maka langsung kirim voice tersebut
        // tanpa menyertakan lampiran-lampiran yang lain
        let voiceDirectSend;
        if (this.isRecordDirectSend) {
            voiceDirectSend = this.chatAttachmentList[this.chatAttachmentList.length - 1];
            this.isRecordDirectSend = false;
        }

        this.dispatchEvent(new CustomEvent('send', { detail: {
            id: crypto.randomUUID(), // Setiap chat harus memiliki Id
            data: {
                text: this.chatInput.value,
                voices: voiceDirectSend ? [voiceDirectSend] : this.chatAttachmentList.filter(x => x.type == 'voice'),
                links: this.chatAttachmentList.filter(x => x.type == 'link'),
                files: this.chatAttachmentList.filter(x => x.type == 'file')
            }
        }}));

        // Kosongkan input dan hapus semua lampiran
        this.chatInput.value = '';
        this.removeAllAttachment();
        this.changePromptMode(ChatPromptMode.NoAttachment);
    }
}