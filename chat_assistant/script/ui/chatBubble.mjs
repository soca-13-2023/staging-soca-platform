import { ChatSender } from "../enum.mjs";

class ChatBubble {
    id = '';
    from = '';
    isLoading = false;
    element = {};

    constructor(id, from, isLoading) {
        this.id = id;
        this.from = from;
        this.isLoading = isLoading;

        // Buat elementnya
        const root = document.createElement('div');
        const profilePicture = document.createElement('img');
        const text = document.createElement('p');

        root.classList.add('chat-bubble', from == ChatSender.User ? 'chat-bubble-user' : 'chat-bubble-server');
        profilePicture.classList.add('chat-bubble-profile-picture');
        text.classList.add('chat-bubble-text');

        profilePicture.src = 'assets/robot.svg';

        // Buat indikator loading apabila chat bubble diatur dalam kondisi loading
        if (isLoading) {
            const loading = document.createElement('img');
            loading.classList.add('chat-bubble-loading');
            loading.src = 'assets/loading.svg';
            root.append(loading);
            this.element = Object.assign(this.element, { loading });
        }

        root.append(profilePicture, text);
        this.element = Object.assign(this.element, { root, profilePicture, text });
    }

    // Isi chat bubble dengan teks yang disediakan
    fill(text) {
        // Apabila chat bubble dalam kondisi loading, hapus indikator loading
        if (this.isLoading) {
            this.element.loading.style.display = 'none';
        }

        this.element.text.innerText = text;
    }
}

export { ChatBubble }