export class ChatService extends EventTarget {
    baseUrl = '';
    

    constructor(baseUrl) {
        super();
        this.baseUrl = baseUrl;
    }

    sendChatText(text) {
        // Id untuk chat bubble jawaban dari Server
        const serverChatBubbleId = crypto.randomUUID();

        // Buat request ke Server untuk mendapatkan jawaban dari chat User,
        // namun sembari menunggu jawaban dari Server kita buat chat bubble dalam kondisi loading terlebih dahulu.
        // Saat Server sudah mengirimkan jawaban, isi chat bubble yang sudah kita buat tadi dengan jawaban dari Server.

        // Perintahkan ChatList untuk membuat chat bubble loading
        this.dispatchEvent(new CustomEvent('message', { detail: {
            id: serverChatBubbleId, data: null
        }}));

        // Buat request ke Server
        $.ajax({
            type: 'POST',
            url: this.baseUrl + '/ask',
            data: JSON.stringify({
                text: text,
                ctype: 'text',
                course_id: ''
            }),
            contentType:'application/json; charset=utf-8',
            dataType: "json",
            success: function(data) {
                // Beri isi chat bubble yang sebelumnya kita buat dengan jawaban dari Server
                this.dispatchEvent(new CustomEvent('message', { detail: {
                    id: serverChatBubbleId,
                    data: { text: data['data']['answers'] }
                }}));
            },
            error: (xhr, status, err) => {
                // Beri isi chat bubble yang sebelumnya kita buat dengan error
                this.dispatchEvent(new CustomEvent('message', { detail: {
                    id: serverChatBubbleId, data: { text: `Error: ${status} ${err}` }
                }}));
            }
        });
    }

    sendChatAudio(userChatBubbleId, voice) {
        const thisClass = this;
        
        // Kirim audio dengan format multipart.
        // Jangan kirim audio lewat JSON karena tampaknya JSON
        // tidak support isi dengan tipe data Blob.
        var formData = new FormData();
        formData.append('file', voice)

        //
        // Buat request ke Server untuk mendapatkan jawaban dari voice chat User.
        // Karena kita tidak dapat mendapatkan jawaban dari voice chat dengan mengirimkan voice chat langsung ke '/ask',
        // kita coba transkripkan voice chat terlebih dahulu melalui '/ask-audio'.
        // Server akan memberikan teks transkrip dari voice chat yang diberikan.
        //
        // Setelah itu kita buat request ke Server lewat '/ask' dengan teks yang telah ditranskripkan sebelumnya.
        //
        // Catatan: Kita tidak perlu membuat chat bubble loading karena sudah dibuatkan oleh ChatPrompt.
        //
        $.ajax({
            type: 'POST',
            url: this.baseUrl + '/ask-audio',
            data: formData,
            processData: false,
            contentType: false,
            success: (data) => {
                // Beri isi chat bubble loading yang dibuat oleh ChatPrompt dengan transkrip teks dari voice chat User
                this.dispatchEvent(new CustomEvent('message', { detail: {
                    id: userChatBubbleId, data: { text: data['data'] }
                }}));

                // Buat request untuk mendapatkan jawaban dari voice chat User
                thisClass.sendChatText(data['data'])
            },
            error: (xhr, status, err) => {
                // Beri isi chat bubble loading yang dibuat oleh ChatPrompt dengan error
                this.dispatchEvent(new CustomEvent('message', { detail: {
                    id: userChatBubbleId, data: { text: `Error: ${status} ${err}` }
                }}));
            }
        });
    }
}