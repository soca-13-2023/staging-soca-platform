function uploadFile() {
    var fileInput = document.getElementById('file-upload');
    var file = fileInput.files[0];

    if (file) {
        var allowedTypes = ["application/pdf", "application/msword", "application/vnd.ms-excel", "text/csv"];
        var maxSize = 25 * 1024 * 1024; // 25MB

        if (allowedTypes.includes(file.type) && file.size <= maxSize) {
            // File valid, tambahkan data ke tabel
            addToTable(file.name, getFileType(file.type), getCurrentTime(), "Process");
            console.log("File is valid. You can proceed with the upload.");
        } else {
            console.log("Invalid file type or size exceeds 25MB.");
        }
    } else {
        console.log("Please select a file.");
    }
}

function addToTable(name, type, updated, status) {
    var table = document.querySelector('.data-table');
    var newRow = table.insertRow(1); // Insert new row at the second position (after the header)

    // Insert cells with data
    newRow.insertCell(0).textContent = name;
    newRow.insertCell(1).textContent = type;
    newRow.insertCell(2).textContent = updated;
    newRow.insertCell(3).innerHTML = '<button class="' + getStatusClass(status) + '">' + status + '</button>';
    newRow.insertCell(4).innerHTML = '<button class="trash"><img src="../img/ic_trash.svg" alt="Delete" /></button>';
}

function getFileType(mimeType) {
    // Implement logic to extract file type from MIME type
    // For simplicity, returning the file extension here
    if (mimeType === "application/pdf") {
        return "pdf";
    } else if (mimeType === "application/msword") {
        return "doc";
    } else if (mimeType === "application/vnd.ms-excel") {
        return "xls";
    } else if (mimeType === "text/csv") {
        return "csv";
    } else {
        return "";
    }
}

function getCurrentTime() {
    var now = new Date();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    return now.getHours() + ':' + (minutes < 10 ? '0' : '') + minutes + ':' + (seconds < 10 ? '0' : '') + seconds + ' ago';
}

function getStatusClass(status) {
    // Implement logic to determine the appropriate class for the status button
    // For simplicity, assuming the class name is the lowercase status value
    return status.toLowerCase();
}

// Delete  Uploud/Submit function
document.addEventListener('DOMContentLoaded', function () {
    var trashButtons = document.getElementsByClassName("trash");

    for (var i = 0; i < trashButtons.length; i++) {
        trashButtons[i].addEventListener('click', function () {
            // Mendapatkan baris (tr) yang sesuai dengan tombol .trash yang diklik
            var row = this.closest('tr');

            // Menghapus baris dari tabel
            row.parentNode.removeChild(row);
        });
    }
}); 

/** 
 * Function toggle active for class="select-files-or-urls"
 */

function toggleActive(element) {
    // Menghapus kelas active dari semua elemen dengan class "select-item"
    var activeElements = document.querySelectorAll('.select-item.active');
    activeElements.forEach(function (el) {
        el.classList.remove('active');
    });

    // Menambahkan kelas active pada elemen yang di-klik
    element.classList.add('active');

    // Menangani tampilan elemen untuk URLs dan Files
    var forUrls = document.querySelector('.for-urls');
    var forFiles = document.querySelector('.for-files');

    if (element.textContent === 'URLs') {
        forFiles.style.display = 'none';
        forUrls.style.display = 'block';
    } else {
        forUrls.style.display = 'none';
        forFiles.style.display = 'block';
    }
}

// Set default display for ".for-files" on page load
window.onload = function () {
    var forFiles = document.querySelector('.for-files');
    forFiles.style.display = 'block';
};