document.addEventListener('DOMContentLoaded', function () {
  // Mengambil elemen checkbox Select All dan semua checkbox pada bagian Capabilities
  const selectAllCheckbox = document.getElementById('selectAll');
  const capabilitiesCheckboxes = document.querySelectorAll('.style-checkbox:nth-child(3) input[type="checkbox"]');

  // Menambahkan event listener untuk checkbox Select All
  selectAllCheckbox.addEventListener('change', function () {
      capabilitiesCheckboxes.forEach(checkbox => {
          if (checkbox.id !== 'publishTo') {
              checkbox.checked = this.checked;
          }
      });
  });

  // Mengambil semua checkbox pada bagian Publish to
  const publishToCheckboxes = document.querySelectorAll('.style-checkbox:nth-child(5) input[type="checkbox"]');

  // Menambahkan event listener untuk setiap checkbox pada bagian Publish to
  publishToCheckboxes.forEach(checkbox => {
      checkbox.addEventListener('change', function () {
          if (this.checked) {
              // Jika checkbox dipilih, hapus centang dari checkbox lainnya pada bagian Publish to
              publishToCheckboxes.forEach(otherCheckbox => {
                  if (otherCheckbox !== this) {
                      otherCheckbox.checked = false;
                  }
              });
          }
      });
  });
});
