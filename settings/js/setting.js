import { getValue, setValue } from "../../preferences/preferences.mjs"

document.addEventListener("DOMContentLoaded" , ()=> {

    const assistantName = document.querySelector('#name');
    const assistantGreeting = document.querySelector('#greeting');
    const assistantDescription = document.querySelector('#description');

    const assistantNamePreview = document.querySelector('.assistant-preview-name');
    const assistantDescriptionPreview = document.querySelector('.assistant-preview-description');
    const assistantInputPreview = document.querySelector('.assistant-preview-input');
    const assistantLogoPreview = document.querySelector(".logo-SocaAssistant")

    assistantName.value = getValue('assistant-name', 'Soca Assistant');
    assistantNamePreview.innerText = getValue('assistant-name', 'Soca Assistant');
    assistantDescription.value = getValue('assistant-description', 'Powerful AI bot to help me work faster and better');
    assistantDescriptionPreview.innerText = getValue('assistant-description', 'Powerful AI bot to help me work faster and better');
    assistantGreeting.value = getValue('assistant-greeting', 'Ada yang bisa Soca bantu?');
    assistantInputPreview.placeholder = getValue('assistant-greeting', 'Ada yang bisa Soca bantu?');

    const logo = getValue('assistant-picture');
    if (logo != '')
      assistantLogoPreview.src = logo;

    assistantName.addEventListener('input', (ev) => {
      assistantNamePreview.innerText = ev.target.value;
      setValue('assistant-name', ev.target.value);
    });
    assistantDescription.addEventListener('input', (ev) => {
      assistantDescriptionPreview.innerText = ev.target.value;
      setValue('assistant-description', ev.target.value);
    });
    assistantGreeting.addEventListener('input', (ev) => {
      assistantInputPreview.placeholder = ev.target.value;
      setValue('assistant-greeting', ev.target.value);
    });

    const inputLogo = document.querySelector(".input-logo")
    const imageLogoAssistant = document.querySelector(".image-logo-assistant")
    inputLogo.addEventListener("change" , (ev) => {
      if (ev.target.files[0].size / 100000 > 2) {
        return alert('Photo too large');
      }

      const fileReader = new FileReader();
      fileReader.onload = ev => {
        imageLogoAssistant.src = ev.target.result;
        assistantLogoPreview.src = ev.target.result;  
        setValue('assistant-picture', fileReader.result);
      }
      fileReader.readAsDataURL(ev.target.files[0]);
    })
})

function pindahKeHalamanSelanjutnya() {
  // Ganti "halaman-selanjutnya.html" dengan nama file halaman selanjutnya.
  window.location.href = "../html/knowledges.html";
}